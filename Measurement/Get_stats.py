f = open('data/' + subject + '.txt','r')
    line = f.readline()
    f.close()
    a = (spliter(line,blocklength))
    length = len(a)

    Pnum = [ 0.0 for i in xrange(blocklength+1) ]
    Qnum = [ 0.0 for i in xrange(blocklength+1) ]
    x = [ i for i in xrange(blocklength+1) ]


    P = [0.0 for i in xrange(length+1)]
    Q = [0.0 for i in xrange(length+1)]
    Qnext = [0.0 for i in xrange(length+1)]
    Pnext = [0.0 for i in xrange(length+1)]


    i = 0
    while(i < length):
        quantity1 = len(re.findall("1",a[i]))
        quantity2 = len(re.findall("01",a[i]))

        if(a[i][0] == '1' and a[i][-1] == '0'):
            quantity2 += 1

        P[i] = quantity1
        Q[i] = quantity2
        Pnum[quantity1] += 1.0
        Qnum[quantity2] += 1.0
        i+=1
return Pnum,Qnum,P,Q
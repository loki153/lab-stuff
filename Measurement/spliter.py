def spliter(string, width):
		return [string[x:x+width] for x in range(0, len(string), width)]

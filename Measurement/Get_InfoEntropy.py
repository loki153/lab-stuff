def Get_Entropy(scale,subject,blocklength):

	# print subject

	f = open('move/' + subject + ' move' +   '/r=' + str(scale) + '.txt', "r")
	line = f.readline()
	# print scale
	a = spliter(line,blocklength)

	length = len(a)


	x = [ i for i in xrange(blocklength+1) ]
	Pnum = [ 0.0 for i in xrange(blocklength+1) ]
	Qnum = [ 0.0 for i in xrange(blocklength+1) ]
	PQnum = [[0.0 for i in range(blocklength/2 + 1)] for i in range(blocklength+1)]
    
    
	P = []
	Q = []
	PQ = []
    
	i = 0




	while(i < length):

		quantity1 = len(re.findall("1",a[i]))
		# pi = quantity1/blocklength
		quantity2 = len(re.findall("01",a[i]))
		if(a[i][0] =='1' and a[i][-1] =='0'):
			quantity2 += 1
		# qi = quantity2/blocklength
		# pqi = pi * qi
		Pnum[quantity1] += 1
		Qnum[quantity2] += 1
		PQnum[quantity1][quantity2] += 1
		i+=1
		
    
	i = 0
	while ( i < len(Pnum)):
		if (Pnum[i] != 0):
			Pentropy = (Pnum[i] / length)  * -(log(Pnum[i] / length))
			P.append(Pentropy)
		if(Qnum[tra] != 0 ):
			Qentropy = (Qnum[i] / length) * -(log(Qnum[i] / length))
			Q.append(Qentropy)
		i += 1

	zoe = 0
	zelda = 0
	while( zoe < blocklength+1):
		while(zelda < (blocklength/2 + 1)):
			if (PQnum[zoe][zelda] != 0.0):
				PQentropy = (PQnum[zoe][zelda] / length) * -(log(PQnum[zoe][zelda] / length))
				PQ.append(PQentropy)
			zelda += 1
		zelda = 0
		zoe += 1



	tropyP = sum(P)
	tropyQ = sum(Q)
	tropyPQ = sum(PQ)

	# print ("r = " + str(scale) + ' '  +  str(tropyP) + '  ' + str(tropyQ) + '	' + str(tropyPQ))

	f.close()

	return tropyP,tropyQ,tropyPQ

def Visual(subject,blocklength,Pnum,Qnum,P,Q):

	x = [ i for i in xrange(blocklength+1) ]
	isExists = os.path.exists(subject +' fig')
	print isExists
	if(isExists == False):
		os.mkdir(subject +' fig')

	fig1 = plt.figure(1)
	ax1 = fig1.add_subplot(111)
	ax1.set_title( subject + '_1dP')
	plt.bar(x,Pnum,color = 'b')
	plt.savefig(subject +' fig/' + subject + '_P.png')


	fig2 = plt.figure(2)
	ax2 = fig2.add_subplot(111)
	ax2.set_title(subject + '_1dQ')
	plt.bar(x,Qnum,color = 'b')
	plt.savefig(subject +' fig/' + subject + '_Q.png')


	fig5 = plt.figure(3)
	ax5 = fig5.add_subplot(111)
	ax5.set_title(subject + '_2dPQ')
	plt.hist2d(P,Q,bins = 40, norm = LogNorm())
	plt.xlim(20,100)
	plt.ylim(15,50)
	plt.colorbar()

	plt.savefig(subject +' fig/' + subject + '_PQ.png')
	plt.show()

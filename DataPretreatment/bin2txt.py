def read_bin(subject):
    f = open('data/' + subject + '.bin','rb')
    line = f.read()
    f.close()
    ordlist = []
    i = 0
    while(i < len(line)):
        ordlist.append(ord(line[i]))
        i += 1

    fw = open('data/' + subject + '.txt','w+')
    i = 0
    while(i < len(ordlist)):
        temp = bin(ordlist[i])
        fw.write(temp[2:])
        i += 1
    fw.close()

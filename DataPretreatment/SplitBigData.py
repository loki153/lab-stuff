def read_in_block(subject):
    BLOCK_SIZE=1024000 #Byte
    i = 0
    with open('data/' + subject + '.bin',"rb+") as f:
        while True:
            block=f.read(BLOCK_SIZE)
            if block:
                i += 1
                fw = open('data/' + subject + '_' + str(i) + '.bin','wb+')
                fw.write(block)
                fw.close()
            else:
                return
